import twitter.Status
import twitter.Notification
import twitter.auth.User
import twitter.auth.UserRole
import grails.converters.JSON
import twitter.auth.Role

class BootStrap {

    def authenticateService

    def init = { servletContext ->

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)

        def user1 = new User(username: 'user1', password: 'user1', realName: 'user1', email: 'user1@gmail.com')
        user1.save(flush: true, failOnError: true)
        UserRole.create user1, userRole, true

        def user2 = new User(username: 'user2', password: 'user2', realName: 'user2', email: 'user2@gmail.com')
        user2.save(flush: true, failOnError: true)
        UserRole.create user2, userRole, true

        def user3 = new User(username: 'user3', password: 'user3', realName: 'user3', email: 'user3@gmail.com')
        user3.save(flush: true, failOnError: true)
        UserRole.create user3, userRole, true

        def user4 = new User(username: 'user4', password: 'user4', realName: 'user4', email: 'user4@gmail.com')
        user4.save(flush: true, failOnError: true)
        UserRole.create user4, userRole, true

        def user5 = new User(username: 'user5', password: 'user5', realName: 'user5', email: 'user5@gmail.com')
        user5.save(flush: true, failOnError: true)
        UserRole.create user5, userRole, true

        //Register User domain for JSON rendering
        JSON.registerObjectMarshaller(User) {
            def output = [:]
            output['id'] = it.id
            output['username'] = it.username
            output['email'] = it.email
            output['realName'] = it.realName
            output['totalTweets'] = it.totalTweets
            output['followings'] = it.followings
            output['followers'] = it.followers

            return output;
        }

        //Register Status domain for JSON rendering
        JSON.registerObjectMarshaller(Status) {
            def output = [:]
            output['id'] = it.id
            output['message'] = it.message
            output['likes'] = it.likes
            output['dateCreated'] = it.dateCreated
            output['author'] = it.author.username

            return output;
        }

        //Register Notification domain for JSON rendering
        JSON.registerObjectMarshaller(Notification) {
            def output = [:]
            output['id'] = it.id
            output['topic'] = it.topic
            output['dateCreated'] = it.dateCreated
            output['message'] = it.message
            output['readed'] = it.readed
            output['replied'] = it.replied
            output['type'] = it.type
            output['user'] = it.messageUser.username

            return output;
        }
    }
    def destroy = {}
}
