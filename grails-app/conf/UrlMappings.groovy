class UrlMappings {

	static mappings = {

        // Home controller
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        "/"{
            controller = "home"
            action = "index"
        }

        // Status controller
        "/status"(controller: "status"){
            action = [GET: 'index']
            format = "json"
        }
        "/status"(controller: "status"){
            action = [POST: 'save']
            format = "json"
        }
        "/status/top-tweets/$max"(controller: "status") {
            action = [GET: 'topTweets']
            format = "json"
        }
        "/status/$id"(controller: "status"){
            action = [GET: 'show']
            format = "json"
        }
        "/status/$id"(controller: "status"){
            action = [PUT: 'update']
            format = "json"
        }
        "/status/$id/like"(controller: "status"){
            action = [GET: 'like']
            format = "json"
        }
        "/status/$id/likes/total"(controller: "status"){
            action = [GET: 'totalLikes']
            format = "json"
        }
        "/status/$id"(controller: "status") {
            action = [DELETE: 'delete']
            format = "json"
        }
        "/status/search-tweet/$tweet"{
            controller = "status"
            action = "searchTweets"
        }
        "/status/search-people"(controller: "status") {
            action = [GET: 'searchPeople']
            format = "json"
        }
        "/edit-profile"{
            controller = "profile"
            action = "editProfile"
        }

        // Profile controller
        "/send-message"(controller: "notification"){
            action = [POST: 'sendMessage']
            format = "json"
        }
        "/users/current-user"(controller: "profile"){
            action = [GET: 'getLoginUser']
            format = "json"
        }
        "/users/$username" (controller: "profile"){
            action = [GET: 'index']
            format = "json"
        }
        "/users/$username/tweets" (controller: "profile"){
            action = [GET: 'tweets']
            format = "json"
        }
        "/users/$username/timeline-tweets" (controller: "profile"){
            action = [GET: 'timelineTweets']
            format = "json"
        }
        "/users/$username/userType" (controller: "profile"){
            action = [GET: 'userType']
            format = "json"
        }
        "/users/$username/follow" (controller: "profile"){
            action = [GET:'followUnfollow']
            format = "json"
        }
        "/users/$username/followings" (controller: "profile"){
            action = [GET: 'followings']
            format = "json"
        }
        "/users/$username/followings/tweets" (controller: "profile"){
            action = [GET: 'followingsTweets']
            format = "json"
        }
        "/users/$username/followers" (controller: "profile"){
            action = [GET: 'followers']
            format = "json"
        }
        "/users/$username/followers/tweets" (controller: "profile"){
            action = [GET: 'followersTweets']
            format = "json"
        }
        "/users/$username/notifications" (controller: "notification"){
            action = [GET: 'notifications']
            format = "json"
        }
        "/users/$username/notifications/$id/delete" (controller: "notification"){
            action = [DELETE: 'deleteNotification']
            format = "json"
        }
        "/users/$username/notifications/$id/read" (controller: "notification"){
            action = [GET: 'markNotificationAsReaded']
            format = "json"
        }
        "/users/$username/messages/$id/read" (controller: "notification"){
            action = [PUT: 'markMessageAsReplied']
            format = "json"
        }
        // Image controller
        "/image/upload"{
            controller = "image"
            action = "upload"
        }
        "/image/show/$userName"{
            controller = "image"
            action = "show"
        }
        "/confirm/$ConfirmCode"{
            controller = "register"
            action = "confirm"
        }
        "/signup"{
            controller = "signup"
            action = "index"
        }
        "500"(view:'/error')
	}
}

