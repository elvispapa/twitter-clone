package twitter

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import twitter.auth.User

@Secured('ROLE_USER')
class StatusController {

    def springSecurityService
    def statusService
    def pushService
    def userService
    def notificationService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Status.list(params), model: [statusCount: Status.count()]
    }

    @Transactional
    def show(Status tweet) {
        if (tweet == null) {
            render status: 404
        } else {
            return [tweet: tweet]
        }
    }

    @Transactional
    def delete(Status tweet) {
        render statusService.delete(tweet)
    }

    def save(Status tweet) {
        tweet.author = getCurrentUser()
        tweet.author.totalTweets++;
        tweet.save(flush: true)

        // Notify all user's followers for the new tweet
        def currentUser = getCurrentUser()
        User.list().each {
            if (currentUser in it.followingUsers) {

                String topic = "/stream/$it.username"
                String message = "$currentUser.username has a new tweet"
                notificationService.saveNotification(topic, message, it, 'notification')
                pushService.pushData(topic, [message: message, user: currentUser, type: 'notification'])
            }
        }
        render tweet.message
    }

    def update(Status tweet) {}

    def getTweets() {
        render currentUserTimeline() as JSON
    }

    def like(Long id) {
        render statusService.like(id, getCurrentUser())
    }

    def totalLikes(Long id) {
        render Status.findById(id).likes
    }

    def currentUserTimeline() {
        def messages = Status.withCriteria {
            or {
                author {
                    eq 'username', currentUser.username
                }
                if (currentUser.followings) {
                    inList 'author', currentUser.followings
                }
            }
            maxResults 10
            order 'dateCreated', 'desc'
        }
        return messages
    }

    def topTweets(Integer max) {
        if (max) {
            def tweets = Status.withCriteria {
                maxResults max
                order 'likes', 'desc'
            }
            render tweets
        }
        render "INPUT_ERROR"
    }

    def searchTweets(String tweet) {
        render Status.findAllByMessageLike("%" + tweet + "%") as JSON
    }

    def searchPeople() {
        def user = getCurrentUser()
        def data = []
        def allUsers = User.where {
            username != user.username
        }.list()
        allUsers.each {
            if (it in user.followingUsers)
                data.add("user": it, "action": "unfollow")
            else
                data.add("user": it, "action": "follow")
        }
        respond data
    }

    private User getCurrentUser() {
        springSecurityService.currentUser
    }
}
