package twitter

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64
import grails.plugin.springsecurity.annotation.Secured
import twitter.auth.User

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

@Secured('ROLE_USER')
class ImageController {

    def springSecurityService
    def userService

    private static final VALID_FORMATS = ['image/png', 'image/jpeg', 'image/gif']

    def upload() {
        // A Spring MultipartFile
        def mpf = request.getFile("file")
        assert mpf

        if (!mpf?.empty && mpf.size < 1024 * 2000000) {

            File dest = new File(
                    "${System.getProperty('user.home')}/twitter/images/profile-images/${getCurrentUser().id}/avatar"         // BETTER to specify a directory in the Grails config file and include it here
            )
            if (!dest.parentFile.exists()) {
                dest.mkdirs()
            }
            mpf.transferTo(dest)
        }
        render "ok"
    }

    def show(String userName) {
        def user = User.findByUsername(userName)
        // Read the profile pic
        def imagePath = "${System.getProperty('user.home')}/twitter/images/profile-images/${user.id}/avatar"

        if (!new File(imagePath).exists()) {      // No profile image exists
            imagePath = "${System.getProperty('user.home')}/twitter/images/profile-images/default"
        }
        BufferedImage image = ImageIO.read(new File(imagePath))
        ByteArrayOutputStream baos = new ByteArrayOutputStream(); ImageIO.write(image, "jpeg", baos);
        String encodedImage = Base64.encode(baos.toByteArray());

        render encodedImage
    }

    def renderImage(String userName) {
        def user = User.findByUsername(userName)
        def imagePath = "${System.getProperty('user.home')}/twitter/images/profile-images/${user.id}/avatar"
        File file = new File(imagePath)

        if (!file.exists()) {      // Profile image doesn't exist
            file = new File("${System.getProperty('user.home')}/twitter/images/profile-images/default")
        }
        response.contentType = 'image/jpeg'
        response.outputStream << file.bytes
        response.flushBuffer()
    }

    def delete() {
        // A Spring MultipartFile
        def mpf = request.getFile("photo")

        println mpf
        def user_id = getCurrentUser.id
        if (!mpf?.empty && mpf.size < 1024 * 200) {
            mpf.transferTo(new File(
                    "/twitter/images/profile-images/${user_id}/avatar.jpg"
            ))
        }
        render UserId
    }

    private User getCurrentUser() {
        springSecurityService.currentUser
    }
}
