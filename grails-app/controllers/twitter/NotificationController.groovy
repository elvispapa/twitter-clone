package twitter

import grails.plugin.springsecurity.annotation.Secured
import twitter.auth.User

@Secured('ROLE_USER')
class NotificationController {

    def springSecurityService
    def notificationService
    def pushService

    def index() {}

    def sendMessage(String receiver, String message) {
        try {
            def receiverInstance = User.findByUsername(receiver)
            def currentUser = getCurrentUser().username
            String topic = "/stream/$receiver"
            String data = "$currentUser: $message"
            notificationService.saveNotification(topic, data, receiverInstance, 'message')
            pushService.pushData(topic, [message: data, user: currentUser, type: 'message'])

            render "OK"
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def notifications(String username) {
        try {
            def userName = username ?: getCurrentUser().username
            def userInstance = User.findByUsername(userName)
            def notifications = Notification.where {
                type == params.type
                user == userInstance
            }.list()

            respond notifications.sort { it.dateCreated }.reverse(true)
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def deleteNotification(Long id) {
        render notificationService.deleteNotification(id)
    }

    def markNotificationAsReaded(Long id) {
        render notificationService.markNotificationAsReaded(id)
    }

    def markMessageAsReplied(Long id) {
        render notificationService.markMessageAsReplied(id)
    }

    private User getCurrentUser() {
        springSecurityService.currentUser
    }
}
