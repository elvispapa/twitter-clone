package twitter

import grails.plugin.springsecurity.annotation.Secured
import twitter.auth.Role
import twitter.auth.User
import twitter.auth.UserRole

@Secured('permitAll')
class RegisterController {

    def mailService

    def index() {}

    def registration() {
        if (params.password != params.password2) {        // Passwords don't match
            render view: "index", model: [serverMessage: "Passwords don't match"]
            return
        }

        def user = User.findByUsername(params.username)     // User exists
        if (user) {
            render view: "index", model: [serverMessage: "There is already an user with this username"]
            return
        }

        // Create a new user object
        def userInstance = new User(username: params.username, password: params.password, realName: params.realName, email: params.email)
        userInstance.save(flush: true, failOnError: true)

        // Assign role to user
        def roleUser = Role.findByAuthority('ROLE_USER')
        UserRole.create userInstance, roleUser, true

        //Send a confirmation email to the new user
        userInstance.enabled = true
        userInstance.accountLocked = false
        userInstance.accountExpired = false
        userInstance.passwordExpired = false

        // Create a new random confirmation code for the user
        userInstance.confirmCode = UUID.randomUUID().toString()

        if (!userInstance.save(flush: true)) {
            return
        }

        mailService.sendMail {
            to userInstance.email
            subject "New User Confirmation"
            html g.render(template: "../templates/mail", model: [code: userInstance.confirmCode])
        }

        render view: "index", model: [serverMessage: "Your account has been creeated. Please confirm your email address. Confirmation link has been sent to your account."]
        redirect(action: "success")

    }

    def success() {
        render(view: 'success', model: [message: 'Your account has been creeated. Please confirm your email address. Confirmation link has been sent to your account']);
    }

    def confirm(String ConfirmCode) {
        User userInstance = User.findByConfirmCode(ConfirmCode)
        if (!userInstance) {
            return;
        }
        // Change the user's confirmation code since it must be used only once
        userInstance.confirmCode = ''

        if (!userInstance.save(flush: true)) {
            render(view: "success", model: [message: 'Problem activating account.'])
            return
        }
        render(view: "success", model: [message: 'You account is successfully activated. Now you can login'])
    }
}
