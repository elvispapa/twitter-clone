package twitter

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import twitter.auth.User

@Secured('ROLE_USER')
class ProfileController {

    def springSecurityService
    def userService
    def notificationService
    def pushService

    def index(String username) {
        def user = User.findByUsername(username)
        if (user)
            respond user
        else
            render "USER_NOT_FOUND"
    }

    def tweets(String username) {
        def user = User.findByUsername(username)
        if (user) {
            respond Status.findAllByAuthor(user).sort { it.dateCreated }.reverse(true)
        } else
            render "USER_NOT_FOUND"
    }

    def timelineTweets(String username) {
        try {
            def timelineTweets = []

            // User's tweets
            def userTweets = Status.findAllByAuthor(User.findByUsername(username)).sort { it.dateCreated }.reverse(true)
            userTweets.each {
                timelineTweets.add(it)
            }

            // User's followings tweets
            User.findByUsername(username).followingUsers.each {
                def followingsTweets = Status.findAllByAuthor(it)
                followingsTweets.each {
                    timelineTweets.add(it)
                }
            }
            respond timelineTweets
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def userType(String username) {
        try {
            def user = User.findByUsername(username)
            if (user in getCurrentUser().followingUsers) {
                render "FOLLOWED"
            } else if (user == getCurrentUser()) {
                render "CURRENT_USER"
            } else {
                render "NOT_FOLLOWED"
            }
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def followUnfollow(String username) {
        try {
            def followingUser = User.findByUsername(username)
            def result = userService.followUnfollow(getCurrentUser(), followingUser)
            if (result == "FOLLOWED") {
                def currentUser = getCurrentUser().username
                String topic = "/stream/$followingUser.username"
                String message = "$currentUser just followed you"
                notificationService.saveNotification(topic, message, followingUser, 'notification')
                pushService.pushData(topic, [message: message, user: currentUser, type: 'notification'])
            } else {
                // Unsubscribe
            }
            render result
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def editProfile() {
        def user = getCurrentUser()
        def data
        if (params.editProfile == "true") {
            // Call the service to edit profile
            def serverMessage = userService.editProfile(user, params)
            data = [user: user, serverMessage: serverMessage]

        } else {      // just show the edit profile page
            data = [user: user, serverMessage: ""]
        }
        render data as JSON
    }

    def followings(String username) {
        try {
            def user = User.findByUsername(username) ?: getCurrentUser()          // if-else
            respond user.followingUsers
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def followingsTweets(String username) {
        try {
            def tweets = []
            def followingUsers = User.findByUsername(username).followingUsers

            followingUsers.each {
                def userTweets = Status.findAllByAuthor(it)
                userTweets.each {
                    tweets.add(it)
                }
            }
            respond tweets
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def followers(String username) {
        try {
            def user = User.findByUsername(username) ?: getCurrentUser()
            def followers = []

            User.list().each {
                if (user in it.followingUsers)
                    followers.add(it)
            }
            respond followers
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def followersTweets(String username) {
        try {
            def user = User.findByUsername(username)
            def followers = []
            User.list().each {
                if (user in it.followingUsers)
                    followers.add(it)
            }
            def tweets = []
            followers.each {
                tweets.add(Status.findAllByAuthor(it))
            }
            respond tweets
        }
        catch (NullPointerException e) {
            render "USER_NOT_FOUND"
        }
    }

    def getLoginUser() {
        render getCurrentUser().username
    }

    private User getCurrentUser() {
        springSecurityService.currentUser
    }
}
