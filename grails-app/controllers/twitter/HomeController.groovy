package twitter

import grails.plugin.springsecurity.annotation.Secured
import twitter.auth.User

@Secured('ROLE_USER')
class HomeController {

    def springSecurityService

    def index() {
        return [user: getCurrentUser()]
    }

    private User getCurrentUser() {
        springSecurityService.currentUser
    }
}
