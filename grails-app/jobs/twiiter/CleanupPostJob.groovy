package twiiter
import grails.plugin.springsecurity.annotation.Secured
import twitter.Status
import twitter.auth.User


class CleanupPostJob {

    def statusService


    static triggers = {
      simple repeatInterval: 10000 // execute job once in 5 seconds
    }

    def execute() {

        def allTweets = Status.list()

        // Check the 'validation' of each tweet
        allTweets.each {
           statusService.checkStatus(it)

        }


    }
}
