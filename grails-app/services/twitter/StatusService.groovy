package twitter

import grails.transaction.Transactional
import twitter.auth.User

@Transactional
class StatusService {

    def mailService
    def springSecurityService
    def words = ["sex","porn","fuck","asholee"]     // Just an example :D

    def checkStatus(Status tweet){

        words.each {
            if(it in tweet.message){
                def statusAuthorEmail = tweet.author.email
                // Delete this tweet
                tweet.delete(flush: true)

                // Send an email to statusAuthor about his/her tweet
                mailService.sendMail {
                    to statusAuthorEmail
                    subject "Harmful content"
                    body "Your tweet $tweet has been deleted due to bad content."

                }
             }
        }
    }

    def like(Long statusId, User currentUser){

        def status = Status.findById(statusId)

        def query = UserStatus.where{
            user == currentUser && tweet == status
        }.find()

        if(query){          // Deslike
            status.likes--
            status.save(flush:true)
            UserStatus.findByTweet(status).delete(flush: true)

            return "DESLIKE"
        }
        else{                   // Like
            def userStatus = new UserStatus(tweet:status, user:currentUser)
            userStatus.save(flush: true, failOnError: true)
            status.likes++
            status.save(flush:true)

            return "LIKE"
        }

    }
    def delete(Status tweet){

        try{
            tweet.delete()
            getCurrentUser().totalTweets--;
            getCurrentUser().save(flush:true, failOnError: true)
            return "OK"
        }
        catch (NullPointerException e){
            return "NOT_OK"
        }
    }

    def create(String tweet, User user){

        // Create a new status
        def status = new Status(author:user, message: tweet)
        status.save(flush: true,failOnError:true)

        checkStatus(status);

        user.totalTweets++
        user.save(flush: true)

        log.info "$user.username just the new tweet $tweet"

        return "OK"
    }

    private User getCurrentUser() {
        springSecurityService.currentUser
    }
}
