package twitter

import com.sun.xml.internal.bind.v2.TODO
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.binding.DataBindingUtils
import twitter.auth.User


@Transactional
class UserService {

    def springSecurityService

    def followUnfollow(User user, User follower){

        // Unfollow
        if(follower in user.followingUsers){

            user.removeFromFollowingUsers(follower)
            user.followings--
            user.save(failOnError: true)

            follower.followers--
            follower.save(failOnError: true)

            log.info "$user removed the follower: $follower"
            return "UNFOLLOWED"

        }
        // Follow
        user.addToFollowingUsers(follower)
        user.followings++
        user.save(failOnError: true)

        follower.followers++
        follower.save(failOnError: true)

        log.info "$user has a new follower: $follower"
        return "FOLLOWED"
    }

    def editProfile(User user, Map params){

        if(params['username'] != getCurrentUser().username){
            // Check if username exists
            if(User.findByUsername(params['username']))
                return "This username is taken, please choose another one"
        }
        DataBindingUtils.bindObjectToInstance(user, params,['username', 'realName', 'email', 'password'], null,null)
        user.save(failOnError: false)
        log.info "$user.username edited his profile"

        // TODO Change also profile image name
        def imagePath = "${System.getProperty('user.home')}/twitter/images/profile-images/${user.id}/avatar"
        File file = new File(imagePath)
        if(! file.exists()){      // No profile image exists
            file = new File("${System.getProperty('user.home')}/twitter/images/profile-images/default")
        }

        return "Your profile was updated successfully"
    }

    private User getCurrentUser() {
        springSecurityService.currentUser
    }
}
