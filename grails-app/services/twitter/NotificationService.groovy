package twitter

import grails.transaction.Transactional
import twitter.auth.User

@Transactional
class NotificationService {

    def springSecurityService

    def saveNotification(String topic, String message, User user, String notificationType){

        def notification = new Notification(topic: topic, message: message, user:user, messageUser: getCurrentUser(), type: notificationType)
        notification.save(failOnError: true)

        user.addToNotifications(notification)
        user.save(failOnError: true)

    }

    def deleteNotification(Long id){

        try{
            Notification.findById(id).delete(flush: true)
            return "OK"
        }
        catch (NullPointerException e){
            return "NOT_OK"
        }
    }

    def markNotificationAsReaded(Long id){
        Notification notification = Notification.findById(id)
        if(!notification || notification.readed){
            return "NOT_OK"
        }
        else{
            notification.readed = true
            notification.save(failOnError: true,flush: true)
            return "OK"
        }
    }

    def markMessageAsReplied(Long id){
        Notification notification = Notification.findById(id)
        if(notification){
            notification.replied = true
            notification.save(failOnError: true, flush: true)
            return "OK"
        }
        else{
            return  "NOT_OK"
        }

    }

    private User getCurrentUser() {
        springSecurityService.currentUser
    }

}
