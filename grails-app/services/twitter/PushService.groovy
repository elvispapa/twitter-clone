package twitter

import grails.converters.JSON
import grails.transaction.Transactional
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import twitter.auth.User

@Transactional
class PushService {

    def pushData(String topic, Map data) {

        def http = new HTTPBuilder("http://localhost:3000/publish")
        Map packet = [topic: topic, data: data]

        http.request(Method.POST, ContentType.JSON) {
            body = (packet as JSON).toString(true)
        }
    }
}
