/**
 * Created by elvis on 6/22/15.
 */

$(document).ready(function () {

    $(".allUsersBtn").on("click", function () {
        if (this.id === "open") {
            $("#close").fadeIn(400);
            $("#allUsers").slideDown();
        }
        else {
            $("#allUsers").slideUp();
            $("#close").fadeOut(400);
        }
    });

    $("#updateStatusBtn").on("click", function () {
        $("#messages").css({"display": "block"});

        setTimeout(function () {
            $("#message").val("");
        }, 1500);
    });

    $("#ClearSearchTweetBtn").on("click", function () {
        $("#tweetToSearch").val("");
    });

    if ($("#messages > div.statusMessage").length === 0) {
        $("#messages").css({"display": "none"});
    }

    $("#searchTweetBtn").on("click", function () {

        $tweetToSearch = $("#tweetToSearch").val();
        if ($tweetToSearch == "") {

            $("#seachFormMessage").empty().append("<b>Please enter a tweet...</b>").fadeIn(2500);
            setTimeout(function () {
                $("#seachFormMessage").fadeOut(3000);
            }, 2000);

            return;
        }
        $.ajax({
            type: "GET",
            url: "/twitter/status/searchForTweet",
            dataType: 'json',
            data: {'tweet': $tweetToSearch},
            success: function (data) {
                console.log(data);
                if (data.length != 0) {
                    $dataToAppend = "";
                    for ($i = 0; $i < data.length; $i++) {

                        $userId = data[$i].userId;
                        $userName = data[$i].username;
                        $tweet = data[$i].tweet;
                        $dateCreated = data[$i].dateCreated;

                        $dataToAppend = $dataToAppend + '<div class="statusMessage">' +
                            '<b><a href="/twitter/profile/' + $userName + '" class="link"> ' + $userName + ' </a></b>' +
                            'said: ' + $tweet.replace('' + $tweetToSearch + '', '<b style="color:#0089D7;">' + $tweetToSearch + '</b>') + '<br/>' +
                            '<div class="statusMessageTime">' +
                            'at <span>' + $dateCreated + '<span/>' +
                            '</div> <hr>' +
                            '</div>';

                    }
                    $("#searchTweetResult").empty().append($dataToAppend);
                }
                else {

                    $("#seachFormMessage").empty().append("<b>No tweets found...</b>").fadeIn(2500);
                    setTimeout(function () {
                        $("#seachFormMessage").fadeOut(3000);
                    }, 2000);
                }
            },
            error: function (request, status, error) {
                console.log(error);
            }
        });

    });
});



