'use strict';

var http = require('http'),
    express = require('express'),
    bodyParser = require('body-parser'),
    faye = require('faye');


var bayeux = new faye.NodeAdapter({
    mount: '/faye',
    timeout: 45
});

var app = express();
var server = http.createServer(app);
bayeux.attach(server);

app.use(express.static(__dirname + '/public'));

//app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));


// Listen for post request(messages) from clients
app.post('/publish', function (request, response) {

    bayeux.getClient().publish(request.body.topic, request.body.data);

    response.writeHead(200, {
        "Content-Type": 'application/json'
    });
    response.write(JSON.stringify({success: true}));
    response.end();
});

var port = process.env.port || 3000;
server.listen(port);
console.log('Server up and listening on port ' + port);



