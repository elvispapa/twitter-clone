//= require jquery/dist/jquery.js
//= require jquery.easing/js/jquery.easing.js
//= require angular/angular.js
//= require angular-route/angular-route.js
//= require angular-sanitize/angular-sanitize.js
//= require angular-utils-pagination/dirPagination.js
//= require restangular/dist/restangular.js
//= require underscore/underscore.js
//= require angular-strap/dist/angular-strap.js
//= require angular-strap/dist/angular-strap.min.js
//= require faye/faye-browser.js
//= require bootstrap
//= require_self
//= require_tree twitter/templates

var twitterApp = angular.module('twitter', ['ngRoute', 'restangular', 'ngSanitize', 'angularUtils.directives.dirPagination', 'mgcrea.ngStrap']);
twitterApp.config(function ($routeProvider, RestangularProvider) {

    // Set base URL
    RestangularProvider.setBaseUrl("http://localhost:8080/twitter/");

    // Set URL mappings
    $routeProvider.
        when('/', {
            templateUrl: 'index.htm',
            controller: 'IndexController'
        }).
        when('/users/:userName', {
            templateUrl: 'profile.htm',
            controller: 'ProfileController'
        }).
        when('/edit-profile', {
            templateUrl: 'edit-profile.htm',
            controller: 'EditProfileController'
        }).
        when('/search-people', {
            templateUrl: 'search-people.htm',
            controller: 'SearchForPeopleController'
        }).
        when('/users/:userName/:action', {
            templateUrl: 'follow.htm',
            controller: 'FollowersFollowingsController'
        }).
        otherwise({
            templateUrl: 'error.htm'
            // controller: 'ErrorHandlingController'
        })
});

// Service for getting the current login user
twitterApp.factory("currentUserInfoFactory", ['Restangular', function (Restangular) {
    return {
        getUserNane: Restangular.one("users", "current-user").get()
    }
}]);

// Service for retrieving user's notifications and deleting/mark as read a tweet
twitterApp.service("notificationsService", ['$http', '$rootScope', function ($http, $rootScope, $parse, Restangular) {

    var self = this;
    self.getUserNotifications = function (object) {
        var jsonObject = JSON.parse(object);
        $http.get("users/" + localStorage.getItem("currentUser") + "/notifications?type=" + jsonObject.type + "").success(function (notifications) {

            if (notifications.length > 0) {
                $rootScope[jsonObject.noItems] = false;
                $rootScope[jsonObject.unreadItemsContainer] = true;
                $rootScope[jsonObject.items] = notifications;

                var totalUnreadItems = 0;
                for (var i = 0; i < notifications.length; i++) {
                    if (!notifications[i].readed)
                        totalUnreadItems++;
                }
                if (totalUnreadItems != 0)
                    $rootScope[jsonObject.totalUnreadItems] = totalUnreadItems;
                else
                    $rootScope[jsonObject.unreadItemsContainer] = false;
            }
            else {
                $rootScope[jsonObject.items] = "";
                $rootScope[jsonObject.noItems] = true;
                $rootScope[jsonObject.unreadItemsContainer] = false;
            }
        });
    }
    self.markNotificationAsRead = function (notificationId, notificationType) {
        $http({
            url: "users/" + localStorage.getItem("currentUser") + "/notifications/" + notificationId + "/read",
            method: "GET"
        }).success(function (result) {
            if (result === "OK") {
                if (notificationType == "message")
                    self.getUserNotifications(localStorage.getItem("messageObjectMapping"));
                else {
                    self.getUserNotifications(localStorage.getItem("notificationObjectMapping"));
                }
            }
        });
    }
    self.markMessageAsReplied = function (id) {
        $http({
            url: "users/" + localStorage.getItem("currentUser") + "/messages/" + id + "/read",
            method: "PUT"
        }).success(function (result) {
            if (result === "OK") {
                var messages = $rootScope.totalUnreadMessages - 1;
                if (messages == 0) {
                    $rootScope.noMessages = true;
                    $rootScope.unreadMessages = false;
                }
                else
                    $rootScope.totalUnreadMessages = messages;
                self.getUserNotifications(localStorage.getItem("messageObjectMapping"));
            }
        });
    }
    self.deleteNotification = function (notificationId, notificationType) {
        $http({
            url: "users/" + localStorage.getItem("currentUser") + "/notifications/" + notificationId + "/delete",
            method: "DELETE"
        }).success(function (data) {
            if (notificationType === "message")
                self.getUserNotifications(localStorage.getItem("messageObjectMapping"));
            else
                self.getUserNotifications(localStorage.getItem("notificationObjectMapping"));
        });
    }
    self.sendMessage = function (receiver, message, repliedMessage) {
        $http({
            url: "/twitter/send-message",
            method: "POST",
            params: {'receiver': receiver, 'message': message}
        }).success(function (data) {
            // If message is a replied message, mark it as "replied"
            if (repliedMessage == true) {
                self.markMessageAsReplied($rootScope.messageId);
            }
            self.getUserNotifications(localStorage.getItem("messageObjectMapping"));
        });
    }
}]);

// Service for uploading files to server
twitterApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function (file, uploadUrl) {
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function (data) {
            })
    }
}]);

// Custom filter for changing DOM element's html
twitterApp.filter("boldSubstring", function ($sce) {
    return function (string, substring) {
        var output = string.replace(substring, "<strong>" + substring + "</strong>");
        return $sce.trustAsHtml(output);
    }
});

// Custom directive for uploading files to server
twitterApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

twitterApp.directive("topTweets", function () {
    return {
        restrict: "E",
        scope: {
            maxResults: "=maxResults"
        },
        controller: function ($scope, $http, Restangular) {

            var initializeDirectiveContent = function () {
                $http({
                    url: "/twitter/status/top-tweets",
                    method: "GET",
                    params: {'max': $scope.maxResults}
                }).success(function (tweets) {
                    $scope.tweets = tweets;
                });
            }
            initializeDirectiveContent();
            $scope.$on("likeTweet", function (event, data) {
                initializeDirectiveContent();
            });
        },
        templateUrl: "top-tweets.htm",
    }
});

twitterApp.directive("followUnfollow", function () {
    return {
        restrict: "E",
        scope: {
            username: "="
        },
        controller: function ($rootScope, $scope, $http, Restangular) {

            var setUserType = function () {
                setTimeout(function () {
                    $http({
                        url: "/twitter/users/" + $scope.username + "/userType",
                        method: "GET"
                    }).success(function (status) {
                        if (status === "FOLLOWED") {
                            $scope.status = "unfollow";
                        }
                        else {
                            $scope.status = "follow";
                        }
                    });
                }, 100);

            }
            setUserType();
            $scope.followUnfollowUser = function (username) {

                Restangular.oneUrl("followUnfollowPeople", "http://localhost:8080/twitter/users/" + username + "/follow").get().then(function (data) {
                    if (data === "FOLLOWED") {
                        $scope.status = "unfollow";
                    }
                    else {
                        $scope.status = "follow";
                    }
                });
                $rootScope.$broadcast("followUser", $scope.username);
            }
        },
        template: "<span class='hoverElem' ng-click='followUnfollowUser(username)' class=''>{{status}}</span>"
    }
});

twitterApp.directive("like", function () {
    return {
        restrict: "E",
        scope: {
            tweetId: "="
        },
        controller: function ($rootScope, $scope, Restangular) {

            var status = Restangular.one("status", $scope.tweetId);
            $scope.like = function (twitterId) {
                status.customGET("like").then(function (action) {                    // status/statusId/like
                    if (action === "LIKE") {
                        $scope.tweetLikes++;
                    }
                    else {
                        $scope.tweetLikes--;
                    }
                    $rootScope.$broadcast("likeTweet", "" + $scope.tweetId + "");
                });
            };
            status.customGET("likes/total").then(function (tweetLikes) {
                $scope.tweetLikes = tweetLikes;
            });
        },
        template: "<div class='hoverElem glyphicon glyphicon-heart likeBtn' ng-click='like(tweetId)'></div> {{tweetLikes}}",
    }
});

twitterApp.directive('slideToggle', function () {
    return {
        restrict: 'A',
        scope: {
            isOpen: "=slideToggle" // 'data-slide-toggle' in our html, true or false
        },
        link: function (scope, element, attr) {
            var slideDuration = parseInt(attr.slideToggleDuration, 10) || 200;

            //Watch for when the value bound to isOpen
            scope.$watch('isOpen', function (newIsOpenVal, oldIsOpenVal) {
                if (newIsOpenVal !== oldIsOpenVal) {
                    element.stop().slideToggle(slideDuration);
                }
            });
        }
    };
});

twitterApp.controller('headerController', function ($rootScope, $scope, Restangular, notificationsService) {

    $scope.showNotifications = true;
    $scope.showMessages = true;

    notificationsService.getUserNotifications(localStorage.getItem("messageObjectMapping"));
    notificationsService.getUserNotifications(localStorage.getItem("notificationObjectMapping"));

    $scope.deleteNotification = function (notificationId, notificationType) {
        notificationsService.deleteNotification(notificationId, notificationType);
        notificationsService.getUserNotifications(localStorage.getItem("messageObjectMapping"));
        notificationsService.getUserNotifications(localStorage.getItem("notificationObjectMapping"));
    }

    $scope.markNotificationAsRead = function (notificationId, notificationType) {
        notificationsService.markNotificationAsRead(notificationId, notificationType);
    }
    $scope.showCurrentUserProfile = function () {
        window.location = "#/users/" + localStorage.getItem("currentUser");
    }

    $scope.saveReceiverAndMessageId = function (username, id) {
        $scope.receiver = username;
        $rootScope.messageId = id;
    }

    $scope.replyToUserMessage = function () {
        notificationsService.sendMessage($scope.receiver, $scope.message, true);
        $("#replyToMessageModal").modal('hide');
    }

});

twitterApp.controller('IndexController', function ($rootScope, $scope, $http, Restangular, notificationsService) {

    // Create a mapping for messages and notifications objects
    var message = {
        type: 'message',
        noItems: 'noMessages',
        unreadItemsContainer: 'unreadMessagesContainer',
        items: 'messages',
        totalUnreadItems: 'totalUnreadMessages'
    };
    var notification = {
        type: 'notification',
        noItems: 'noNotifications',
        unreadItemsContainer: 'unreadNotificationsContainer',
        items: 'notifications',
        totalUnreadItems: 'totalUnreadNotifications'
    };
    localStorage.setItem("messageObjectMapping", JSON.stringify(message));
    localStorage.setItem("notificationObjectMapping", JSON.stringify(notification));

    var getUserTweets;

    Restangular.one("users", "current-user").get().then(function (username) {
        // Save current user
        $rootScope.currentUser = username;
        localStorage.setItem("currentUser", username);
    });

    setTimeout(function () {

        notificationsService.getUserNotifications(localStorage.getItem("notificationObjectMapping"));
        notificationsService.getUserNotifications(localStorage.getItem("messageObjectMapping"));

        // Subscribe and listen for notifications
        var client = new Faye.Client('http://localhost:3000/faye', {
            timeout: 20
        });
        client.subscribe("/stream/" + localStorage.getItem("currentUser") + "", function (notification) {
            console.log(notification)
            // When received a notification, trigger the appropriate notifications on the dashboard based on the type
            if (notification.type === "message")
                notificationsService.getUserNotifications(localStorage.getItem("messageObjectMapping"));
            else
                notificationsService.getUserNotifications(localStorage.getItem("notificationObjectMapping"));
        });

        getUserTweets = function () {
            Restangular.one("users", localStorage.getItem("currentUser")).all("timeline-tweets").getList().then(function (userTweets) {
                $scope.tweets = userTweets;
            });
        }
        getUserTweets();
    }, 100)

    $scope.createTweet = function () {
        var newTweet = {message: $scope.tweetToAdd};
        Restangular.all("status").post(newTweet).then(function (data) {
            $scope.tweetToAdd = "";
            getUserTweets();
        });
    };

    $scope.searchForTweet = function () {

        if ($scope.query === " " || $scope.query === undefined) {
            $scope.queryResults = [];
            $scope.totalResults = "";
            return;
        }
        var status = Restangular.one("status", "search-tweet");
        status.customGET($scope.query).then(function (queryResults) {
            console.log(queryResults)
            $scope.queryResults = queryResults;
            if (queryResults.length == 0) {
                $scope.totalResults = "";
            }
            else {
                $scope.totalResults = queryResults.length + " tweets found:";
            }
        });
    };

    $scope.$on("likeTweet", function (event, data) {
        getUserTweets();    // Update tweets
    });
});

twitterApp.controller('ProfileController', function ($rootScope, $scope, $routeParams, Restangular, $http, $location, notificationsService) {

    var user = Restangular.one("users", $routeParams.userName);     // users/user1
    var getProfileInfo = function () {

        // Get user's profile data
        user.get().then(function (user) {
            if (user === "USER_NOT_FOUND") {
                $location.path('/404');
            }
            $scope.user = user;
        });
    };
    var getProfileTweets = function () {

        // Check if the current use is followed or not
        user.customGET("userType").then(function (result) {

            if (result === "FOLLOWED") {
                user.all("tweets").getList().then(function (tweets) {
                    $scope.tweets = tweets;
                });
                $scope.action = "unfollow";
                $scope.outerUser = true;
                $scope.currentUser = false;
            }
            else if (result === "NOT_FOLLOWED") {
                $scope.tweets = "";
                $scope.action = "follow";
                $scope.outerUser = true;
                $scope.currentUser = false;
            }
            else if (result === "CURRENT_USER") {
                // Get user's tweets
                user.all("tweets").getList().then(function (tweets) {
                    $scope.tweets = tweets;
                });
                $scope.outerUser = false;
                $scope.currentUser = true;
            }
            else
                $location.path('/404');
        });
    }

    getProfileInfo();
    getProfileTweets();

    $scope.$on("followUser", function (event, data) {
        setTimeout(function () {
            getProfileInfo();
            getProfileTweets();
        }, 100)

    });

    $scope.deleteTweet = function () {

        var tweet = Restangular.one("status", $scope.tweetToDeleteId);
        tweet.remove().then(function (result) {
            console.log("RESULT IS:" + result);
            getProfileInfo();
            getProfileTweets();
        });
    };

    $scope.saveTweetId = function (tweetId) {
        $scope.message = "";
        $scope.tweetToDeleteId = tweetId;
    }

    $scope.sendMessageToUser = function () {
        notificationsService.sendMessage($scope.receiver, $scope.message, false);
        $("#sendMessageModal").modal('hide');
    }
});

twitterApp.controller('EditProfileController', function ($http, $scope, fileUpload, notificationsService) {

    $http({
        url: "/twitter/edit-profile",
        method: "GET",
        params: {'editProfile': false}
    }).success(function (data) {

        $scope.username = data.user.username;
        $scope.email = data.user.email;
        $scope.realName = data.user.realName;
        $scope.serverMessage = data.serverMessage

    });

    $scope.changeCredentials = function () {

        if ($scope.password1 != $scope.password2) {
            $scope.serverMessage = "Passwords must match.";
            setTimeout(function () {
                $("#serverMessage").fadeOut(2000);
            }, 2000);
            return;
        }
        else {
            $http({
                url: "/twitter/edit-profile",
                method: "GET",
                params: {
                    'username': $scope.username,
                    'realName': $scope.realName,
                    'email': $scope.email,
                    'password': $scope.password,
                    'editProfile': true
                }
            }).success(function (data, status, headers, config) {
                console.log("GOT DATA 2", data);
                $scope.user = data.user;
                $scope.serverMessage = data.serverMessage;
                setTimeout(function () {
                    $("#serverMessage").fadeOut(2000);
                }, 2000);
            });
        }
    };
    $scope.changeProfileImage = function () {
        console.log("changeProfileImage");
        var file = $scope.myFile;
        if (file.type !== "image/jpeg") {
            alert("Please choose a .jpg img");
            return;
        }
        var uploadUrl = "/twitter/image/upload";
        fileUpload.uploadFileToUrl(file, uploadUrl);
    };
});

twitterApp.controller('FollowersFollowingsController', function ($http, $scope, $routeParams, Restangular, notificationsService) {

    var user = Restangular.one("users", $routeParams.userName);
    user.all($routeParams.action).getList().then(function (users) {
        $scope.users = users;
    });
});

twitterApp.controller('SearchForPeopleController', function ($scope, $http, Restangular, notificationsService) {

    Restangular.oneUrl("searchForPeople", "http://localhost:8080/twitter/status/search-people").get().then(function (users) {
        $scope.users = users;
    });
});

$(document).ready(function () {

    $(".deleteTweetBtn").on("click", function () {
        $(this).remove();
    });

});























