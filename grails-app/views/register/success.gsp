<div class="container">
    <h1>Registration</h1>
    <div class="alert alert-success">
        ${message}
    </div>
    <g:link controller="login" action="auth">
        Login
    </g:link>
</div>