<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to twitter </title>

		<link rel="stylesheet" href="${resource(dir: 'css', file: 'index.css')}" type="text/css">
        <style>
            #homePageBtns{
                width: 200px;
                margin-top: 200px;
                margin:auto;
            }
            .button{
                color:white !important;
            }

        </style>
	</head>
	<body>

        <div id="homePageBtns">
            <g:link class="button"  controller="login" action="index">Login</g:link>
            <g:link class="button"  controller="register" action="index">Register</g:link>
        </div>
	</body>
</html>
