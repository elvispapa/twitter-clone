<header class="">
    <div id="logo">
        <h2>Welcome user!</h2>
    </div>

    <sec:ifLoggedIn>
        <div id='menu' class="btn-group" role="group" aria-label="...">
            <div id="menuCol1">
                <div id="notificationsBtn" class="icon">
                    <a href="">
                        <span class="glyphicon glyphicon-bell"></span>
                    </a>
                </div>
                <div id="totalNotifications">
                    2
                </div>
            </div>
            <div id="menuCol2">
                <a type="button" class="btn btn-default" href="/twitter/#/">
                    <div>
                        <span class="glyphicon glyphicon-home"></span> <span>Home</span>
                    </div>
                </a>

                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        My Profile
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <i class="icon-arrow-up"></i>
                            <a href="/twitter/#/users/${user?.username}">
                                <span class="glyphicon glyphicon-user"></span><span>My Profile</span>
                            </a>
                        </li>
                        <li>
                            <a  href="#/edit-profile"> <span class="glyphicon glyphicon-cog"></span> Edit Profile </a>
                        </li>
                        <li>
                            <g:link controller="logout" action="index"> <span class="glyphicon glyphicon-off"></span> Log out</g:link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </sec:ifLoggedIn>
</header>