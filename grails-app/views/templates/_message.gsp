<div class="statusMessage">

    <strong>
        <g:link controller="profile" params="[username:message.author.username]" class="link">
            ${message.author.realName}
        </g:link>
    </strong>   said:
    ${message.message} <br/>

    <div class="statusMessageTime">
        at <g:formatDate date='${message.dateCreated}' />
    </div>
    <hr>

</div>