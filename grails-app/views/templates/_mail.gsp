<div>
    <h1> Email Confirmation</h1>

    <g:link absolute="true" controller="register" action="confirm" id="${code}">
        Please click this link to confirm your account
    </g:link>

    <g:createLink absolute="true" controller="register" action="confirm" id="${code}"></g:createLink>
</div>