package twitter

import twitter.auth.User

class Notification {

    String topic
    String message
    String type
    User messageUser
    Date dateCreated
    Boolean readed = false
    Boolean replied = false

    static belongsTo = [user: User]
    static constraints = {
    }
}
