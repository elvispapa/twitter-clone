package twitter

import grails.transaction.Transactional
import twitter.auth.User

@Transactional(readOnly = true)
class UserStatus {

    User user
    Status tweet

    static constraints = {
    }
}
