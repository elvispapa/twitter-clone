package twitter.auth

import grails.transaction.Transactional
import twitter.Notification

@Transactional(readOnly = true)
class User {

	transient springSecurityService

	String username
	String realName
	String password
	String email
    String confirmCode
    int totalTweets = 0
    int followers = 0
    int followings = 0

	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static hasMany = [followingUsers : User, notifications : Notification]

	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password blank: false
		realName blank: false
        email blank: false, email:true
        confirmCode nullable: true
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}
}
