package twitter

import grails.rest.*
import grails.transaction.Transactional
import twitter.auth.User

@Transactional(readOnly = true)
class Status {

    String message
    Date dateCreated
    int likes = 0
    static belongsTo = [author:User]

    static constraints = {}
}
