// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//



// angular code goes here


var twitterApp = angular.module('twitterApp',['ngRoute']);

twitterApp.config(function ($routeProvider) {
    $routeProvider

        .when('/',{
            templateUrl:'templates/index.html',
            controller:'indexController'
        })
        .when('/profiles/:userName',{
            templateUrl:'/templates/profile.html',
            controller:'profileController'
        })
        .when('/followings/:userName',{
            templateUrl:'/templates/profile.html',
            controller:'profileController'
        })
        .otherwise({
            redirectTo:'/'
        });
});


twitterApp.controller('indexController',function($scope,$http){

    $http({
        url: "http://localhost:8080/twitter/status/index",
        method: "GET"
    }).success(function(data, status, headers, config) {
        console.log(data);
        $scope.firstName = "OK";

    }).error(function(data, status, headers, config) {
        console.log(data);
    });

});

twitterApp.controller('profileController',function(){

});
twitterApp.controller('followController',function($scope,$routeParams){


    $scope.follow = function(action,userId) {

        var url;
        if(action === "NOT_FOLLOWED")
            url = "/profile/follow/"+userId+"";
        else
            url = "/profile/unfollow/"+userId+"";
        $http.get(url).
            success(function(data) {
                console.log(data);
            });
    };
});



